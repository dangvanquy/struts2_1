<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>register</title>
    <link rel="stylesheet" href='<c:url value="../../resources/css/bootstrap.css"/> '/>
    <link rel="stylesheet" href='<c:url value ="../../resources/css/freshfood.css"/>'/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
</head>
<body>

<s:form action="register" method="POST">
    <s:textfield type="text" name="name" label="Name"/>
    <s:textfield type="text" name="email" label="Email"/>
    <s:password name="password" label="Password"/>
    <s:textfield type="text" name="address" label="Address"/>
    <s:textfield type="text" name="phone" label="Phone"/>
    <s:submit label="Register"/>
</s:form>

</body>
</html>