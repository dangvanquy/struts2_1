package com.quycntt.daoimp;

import com.quycntt.dao.UserDao;
import com.quycntt.entity.User;
import org.hibernate.*;

public class UserImp implements UserDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User getUser(String email, String password) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM User u WHERE u.email=:email and u.password=:password");
        query.setParameter("email", email);
        query.setParameter("password", password);
        User user = (User) query.uniqueResult();
        return user;
    }

    public boolean register(User user) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        boolean check = false;

        int id = (Integer) session.save(user);
        if (id > 0) {
            check = true;
        } else {
            check = false;
        }
        return check;
    }
}