package com.quycntt.daoimp;

import com.quycntt.dao.FoodDao;
import com.quycntt.entity.Food;
import org.hibernate.*;

import java.util.List;

public class FoodImp implements FoodDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Food> getListFood() {
        List<Food> listFood = null;
        listFood = getFood(0, 3);
        return listFood;
    }

    public List<Food> getListFoodNew() {
        List<Food> listFood = null;
        listFood = getFood(3, 4);
        return listFood;
    }

    public List<Food> getListFoodDescription() {
        List<Food> listFood = null;
        listFood = getFood(7, 3);
        return listFood;
    }

    public Food getFood1(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        Food food = new Food();

        try {
            transaction = session.beginTransaction();
            String sql = "from food where id = "+id;
            food = (Food) session.createQuery(sql).uniqueResult();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }

        return food;
    }

    private List<Food> getFood(int first, int max) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        List<Food> listFood = null;

        try {
            transaction = session.beginTransaction();
            String sql = "FROM food";
            listFood = (List<Food>) session.createQuery(sql).setFirstResult(first).setMaxResults(max).list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }

        return listFood;
    }
}