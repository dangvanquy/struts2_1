package com.quycntt.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.daoimp.UserImp;
import com.quycntt.entity.Cart;
import com.quycntt.entity.User;
import com.quycntt.service.UserService;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;

public class LoginAction extends ActionSupport implements SessionAware{
    private UserService userService;
    private User user;
    private String email;
    private String password;
    private Map<String, Object> session;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public String execute() throws Exception {
        return INPUT;
    }

    public String login() {
        user = userService.getUser(email, password);
        if (user != null) {
            session.put("users", user);
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    public String logout() {
        if (this.session.containsKey("users")) {
            this.session.remove("users");
            Map session = ActionContext.getContext().getSession();
            session.remove("listcart");
            session.remove("sum");
        }
        return SUCCESS;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}