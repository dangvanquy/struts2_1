package com.quycntt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.User;
import com.quycntt.service.UserService;

public class RegisterAction extends ActionSupport {
    private UserService userService;
    private String name;
    private String email;
    private String password;
    private String address;
    private String phone;
    private boolean check;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String register() {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        check = userService.register(user);

        if (check) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }
}