package com.quycntt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Food;
import com.quycntt.entity.Menu;
import com.quycntt.service.FoodService;
import com.quycntt.service.MenuService;

import java.util.List;

public class IndexAction extends ActionSupport{
    private MenuService menuService;
    private FoodService foodService;
    private List<Menu> listMenu;
    private List<Food> listFood;
    private List<Food> listFood1;
    private List<Food> listFood2;

    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    public void setFoodService(FoodService foodService) {
        this.foodService = foodService;
    }

    public MenuService getMenuService() {
        return menuService;
    }

    public List<Menu> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<Menu> listMenu) {
        this.listMenu = listMenu;
    }

    public List<Food> getListFood() {
        return listFood;
    }

    public void setListFood(List<Food> listFood) {
        this.listFood = listFood;
    }

    public List<Food> getListFood1() {
        return listFood1;
    }

    public void setListFood1(List<Food> listFood1) {
        this.listFood1 = listFood1;
    }

    public List<Food> getListFood2() {
        return listFood2;
    }

    public void setListFood2(List<Food> listFood2) {
        this.listFood2 = listFood2;
    }

    @Override
    public String execute() throws Exception {
        this.listMenu = menuService.getListMenu();
        this.listFood = foodService.getListFood();
        this.listFood1 = foodService.getListFoodNew();
        this.listFood2 = foodService.getListFoodDescription();

        return SUCCESS;
    }
}