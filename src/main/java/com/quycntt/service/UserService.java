package com.quycntt.service;

import com.quycntt.dao.UserDao;
import com.quycntt.daoimp.UserImp;
import com.quycntt.entity.User;

public class UserService implements UserDao {
    private UserImp userImp;

    public void setUserImp(UserImp userImp) {
        this.userImp = userImp;
    }
    public User getUser(String email, String password) {
        return userImp.getUser(email, password);
    }

    public boolean register(User user) {
        return userImp.register(user);
    }
}