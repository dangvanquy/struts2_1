package com.quycntt.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "menu")
public class Menu {
    @Id
    private int id;
    private String name;
    private String link;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}