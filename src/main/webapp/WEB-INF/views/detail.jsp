<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>detail</title>
    <link rel="stylesheet" href='<c:url value="../../resources/css/bootstrap.css"/> '/>
    <link rel="stylesheet" href='<c:url value ="../../resources/css/freshfood.css"/>'/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="icondetail">
        <div id="alertsuccess" class="alert alert-success">
            <strong>Đã Thêm</strong>
        </div>
        <a href="cartexe"><i class="fa fa-shopping-cart"></i></a>
    </div>
    <div class="row">
        <div id="chitietimg" class="col-lg-4 col-md-4">
            <p id="id" data-id="<c:out value="${food.getId()}"/>"></p>
            <img src='resources/<c:url value="${food.getImage()}"/> '>
        </div>
        <div id="chitiet" class="col-lg-4 col-md-4">
            <p id="name1"><c:out value="${food.getName()}"/></p>
            <div class="row">
                <div class="col-md-3">
                    <p>Giá</p>
                </div>
                <div class="col-md-9">
                    <p id="price" data-price="<c:out value="${food.getPrice()}"/>"><c:out value="${food.getPrice()}"/>VNĐ</p>
                    <p id="quantity" data-quantity="<c:out value="${food.getQuantity()}" />"></p>
                </div>
            </div>
            <button id="btngiohang" class="btn btn-primary">Giỏ Hàng</button>
        </div>
        <div class="col-lg-4 col-md-4">
            <div id="thongtin">
                <h3>Thông tin</h3>
                <p><c:out value="${food.getDescription()}"/></p>
            </div>
        </div>
    </div>
</div>

<script src="../../resources/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $("#btngiohang").click(function () {
        var id = $("#id").attr("data-id");
        var name = $("#name1").text();
        var price = $("#price").attr("data-price");
        var dataJsonObj = {"id":id,"name":name,"price":price,"quantity":1};

        $.ajax({
            url:'detail',
            type:'GET',
            data:{ dataJson: JSON.stringify(dataJsonObj)},
            success: function () {
                $("#alertsuccess").fadeIn();
                $("#alertsuccess").fadeOut(2000);
            },
            error: function () {
            }
        });
    });
</script>
</body>
</html>